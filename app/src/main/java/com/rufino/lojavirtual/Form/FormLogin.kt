package com.rufino.lojavirtual.Form

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.rufino.lojavirtual.MainActivity
import com.rufino.lojavirtual.databinding.ActivityFormLoginBinding

class FormLogin : AppCompatActivity() {

    private lateinit var binding: ActivityFormLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide()

        binding.textTelaCadastro.setOnClickListener {
            var intent = Intent(this, FormCadastro::class.java)
            startActivity(intent)
        }

        binding.entrar.setOnClickListener {
            var usuario = binding.usuario.text.toString()
            var senha = binding.senha.text.toString()

            if (usuario.isEmpty() || senha.isEmpty()) {
                Toast.makeText(this, "Preencha os campos usuário e senha", Toast.LENGTH_SHORT)
                    .show()
            } else {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(usuario, senha)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            Toast.makeText(this, "Realizado login com sucesso", Toast.LENGTH_SHORT)
                                .show()
                            var intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            Toast.makeText(this, "Falha ao realizar login", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
            }
        }
    }
}